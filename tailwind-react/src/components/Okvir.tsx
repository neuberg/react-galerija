import React, { ChangeEvent, FormEvent } from 'react';
import { useParams } from 'react-router-dom';
import Izbrisi from './Izbrisi';




interface Okvir{
  url:string,
    Ime_Slike:string,
    Avtor:string,
    Leto:string
    Zasciteni:boolean,
    prodan:boolean,
    kategorija:string,
    dimenzije:string
}


interface Okvirr{
  okvir:Okvir[];
  onChange: (okvirji: Okvir[]) => any;  
}

function Okvir (props:Okvirr){

  let { id }:any = useParams();

  const [okvir,setOkvir] = React.useState<Okvir>(props.okvir[id]);

 

const handleElementClick = () => {
    let okv = { ...okvir };
    okv.Zasciteni = !okv.Zasciteni;
    setOkvir(okv);
    let okvZas = props.okvir;
    okvZas[id] = okv;
    props.onChange(okvZas);
}



const handleElementClick1 = () => {
  let okv = { ...okvir };
  okv.prodan = !okv.prodan;
  setOkvir(okv);
  let okvZas = props.okvir;
  okvZas[id] = okv;
  props.onChange(okvZas);
}




const [url, setUrl] = React.useState<string>(okvir.url);
const [ime, setIme] = React.useState<string>(okvir.Ime_Slike);
const [avtor, setAvtor] = React.useState<string>(okvir.Avtor);    
const [leto, setLeto] = React.useState<string>(okvir.Leto);
const [zascitena, setZascitena] = React.useState<boolean>(okvir.Zasciteni);
const [prodan, setProdan] = React.useState<boolean>(okvir.Zasciteni);
const [kategorija, setKategorija] = React.useState<string>(okvir.Ime_Slike);
const [dimenzija, setDimenzija] = React.useState<string>(okvir.Ime_Slike);

const handleChange_ = (e: ChangeEvent<HTMLInputElement>) => {
  setUrl(e.target.value);
}

const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
  setIme(e.target.value);
}
const handleChange1 = (e: ChangeEvent<HTMLInputElement>) => {
  setAvtor(e.target.value);
}    
const handleChange2 = (e: ChangeEvent<HTMLInputElement>) => {
  setLeto(e.target.value);
}

const handleChangeK = (e: ChangeEvent<HTMLInputElement>) => {
  setKategorija(e.target.value);
}    
const handleChangeD = (e: ChangeEvent<HTMLInputElement>) => {
  setDimenzija(e.target.value);
}


const posodobi = (e: FormEvent) => {
  e.preventDefault();
  let novokvir: Okvir = {
    url:url,
    Ime_Slike: ime,
    Avtor: avtor,
    Leto: leto,
    Zasciteni: zascitena,
    prodan:prodan,
    kategorija:kategorija,
    dimenzije:dimenzija
         
  }
 setOkvir(novokvir);
 let novo = props.okvir;
 novo[id] = novokvir;
 props.onChange(novo);
}  




const [uredi, setUredi] = React.useState(false);
const onClick = () => {
  setUredi(true);    
    let okv = {...okvir};
    setOkvir(okv);
}



    return (
      <main className="profile-page">
      <section className="relative block" style={{ height: "500px" }}>
        <div
          className="absolute top-0 w-full h-full bg-center bg-cover"
          style={{
            backgroundImage:
              "url('https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2710&q=80')"
          }}
        >
          <span
            id="blackOverlay"
            className="w-full h-full absolute opacity-50 bg-black"
          ></span>
        </div>
        <div
          className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
          style={{ height: "70px", transform: "translateZ(0)" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
           
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-gray-300 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
      </section>
      <section className="relative py-16 bg-gray-300">
        <div className="container mx-auto px-4">
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
            <div className="px-6">
              <div className="flex flex-wrap justify-center">
                <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                  <div className="relative">
                    <img
                      alt=""
                      
                      className="shadow-xl rounded-full h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16"
                      style={{ maxWidth: "150px" }}
                    />
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4 lg:order-3 lg:text-right lg:self-center">
                  <div className="py-6 px-3 mt-32 sm:mt-0">
                    
                  <div>
            <input type="submit" value="UREJANJE" onClick={onClick} /><br></br>
            { uredi ?
            <div>
                <form onSubmit={posodobi}>
               <ul>
                <label>URL slike: 
                 <input type="text" value={url} onChange={handleChange_}/><br></br>
                 </label>
                 <label>Naslov slike: 
                 <input type="text" value={ime} onChange={handleChange}/><br></br>
                 </label>
                 
                 <label>Avtor: 
                 <input type="text" value={avtor} onChange={handleChange1}/><br></br>
                 </label>          
                 <label>Leto: 
                 <input type="text" value={leto} onChange={handleChange2}/><br></br>
                 </label>
                 <label>Kategorija: 
                 <input type="text" value={kategorija} onChange={handleChangeK}/><br></br>
                 </label>
                 <label>Dimenzije: 
                 <input type="text" value={dimenzija} onChange={handleChangeD}/><br></br>
                 </label>
                 </ul> 
                
            
          
                <input type="submit" value="Posodobi" />
                </form><br></br><br></br>
            </div>
             : null }
            </div>

                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4 lg:order-1">
                  <div className="flex justify-center py-4 lg:pt-4 pt-8">
                    <div className="mr-4 p-3 text-center">
                      <span className="text-xl font-bold block uppercase tracking-wide text-gray-700">
                        Avtor
                      </span>
                      <span className="text-sm text-gray-500">{okvir.Avtor}</span>
                    </div>
                    <div className="mr-4 p-3 text-center">
                      <span className="text-xl font-bold block uppercase tracking-wide text-gray-700">
                        Leto
                      </span>
                      <span className="text-sm text-gray-500">{okvir.Leto}</span>
                    </div>
                    <div className="lg:mr-4 p-3 text-center">
                      <span className="text-xl font-bold block uppercase tracking-wide text-gray-700">
                        Dimenzija
                      </span>
                      <span className="text-sm text-gray-500">{okvir.dimenzije}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="text-center mt-12">
                <h3 className="text-4xl font-semibold leading-normal mb-2 text-gray-800 mb-2">
                  {okvir.Ime_Slike} 
                </h3>
                </div>
              

                <div className="text-center mt-12">
                <h3 className="text-4xl font-semibold leading-normal mb-2 text-gray-800 mb-2">
              
                <img src={okvir.url} width="300"  height="300"/> 
             
                </h3>
                </div>
                
              
              <div className="mt-10 py-10 border-t border-gray-300 text-center">
                <div className="flex flex-wrap justify-center">
                  <div className="w-full lg:w-9/12 px-4">
                    <p className="mb-4 text-lg leading-relaxed text-gray-800">
                    <div className="font-bold text-black-500 text-sm mb-2">
          {okvir.Zasciteni? "Slika je zaščitena": "Slika ni zaščitena"} <br></br>
            <input type="submit" value="ZAŠČITI" onClick={handleElementClick}/><br></br><br></br>
            
            
            {okvir.prodan? "Slika je prodana": "Slika ni prodana"} <br></br>
            <input type="submit" value="PRODAJ" onClick={handleElementClick1}/><br></br>
        
        </div>
                    </p>
                    <a
                      href="#pablo"
                      className="font-normal text-pink-500"
                      onClick={e => e.preventDefault()}
                    >
                     
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    
    );
}


export default Okvir;