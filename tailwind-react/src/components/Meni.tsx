import React, { ChangeEvent } from 'react';
import { Link } from 'react-router-dom';
import images from './slike';




interface Okvir{
    url:string
    Ime_Slike:String,
    Avtor:String,
    Leto:String,
    Zasciteni:boolean,
    prodan:boolean,
    kategorija:string,
    dimenzije:string
}


interface GlavniMeni{
    okvirji:Okvir[];
  }

function Meni (props:GlavniMeni){

    const {okvirji} = props; 


    const [filter, setFilter] = React.useState<string>("");  
    const [list, setList] = React.useState(okvirji);
    

    const handleChangeFilter = (e: ChangeEvent<HTMLInputElement>) => {
        setFilter(e.target.value);
    }
   
    
    return (
        
        <main className="profile-page">
             <div className="font-bold text-black-500 text-xl mb-2 align-middle">
             Filtriranje: <input type="text" className="form-input mt-1 block" value={filter} onChange={handleChangeFilter}></input><br></br>  
             </div>    
        <section className="relative block" style={{ height: "500px" }}>
          
          <div
            className="absolute top-0 w-full h-full bg-center bg-cover"
            style={{
              backgroundImage:
                "url('https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2710&q=80')"
            }}
          >
            <span
              id="blackOverlay"
              className="w-full h-full absolute opacity-50 bg-black"
            ></span>
          </div>
          
          
          <div
            className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
            style={{ height: "70px", transform: "translateZ(0)" }}
          >
            <svg
              className="absolute bottom-0 overflow-hidden"
             
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="text-gray-300 fill-current"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </section>
        
        <section className="relative py-16 bg-gray-300">
          <div className="container mx-auto px-4">
              
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
              <div className="px-6">
                <div className="flex flex-wrap justify-center">
                  <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                    <div className="relative">
                    
                    </div>
                  </div>
                  <div className="w-full lg:w-4/12 px-4 lg:order-3 lg:text-right lg:self-center">
                    <div className="py-6 px-3 mt-32 sm:mt-0">
                     
                    </div>
                  </div>
                  <div className="w-full lg:w-4/12 px-4 lg:order-1">
                  <div className="font-bold text-black-500 text-xl mb-2 align-middle">
                  

                  <ul>
                  {list.filter(i => i.kategorija.includes(filter)).map((el,id)=> (

                  <>
                   <li><Link to={`/okvir/${id}`}>Ime slike: {el.Ime_Slike} | Avtor: {el.Avtor} | Kategorija:{el.kategorija}</Link><img src={el.url} width="300" height="300" /></li>   
                  </>
                  ))}
                  </ul>
                  </div>
                  </div>
                </div>
                <div className="text-center mt-12">
                  <h3 className="text-4xl font-semibold leading-normal mb-2 text-gray-800 mb-2">
                    
                  </h3>
                  </div>
                
  
                  <div className="text-center mt-12">
                  
                
              
               
             
                  </div>
                  
                
               
                      <div className="font-bold text-black-500 text-sm mb-2">
          
          
 
                     
                      <a
                        href="#pablo"
                        className="font-normal text-pink-500"
                        onClick={e => e.preventDefault()}
                      >
                       
                      </a>
                    </div>
                  </div>
                </div>
             
         
          </div>
        </section>
      </main>
      
      
    );
}




export default Meni;



