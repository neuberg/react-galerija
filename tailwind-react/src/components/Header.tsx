import React from 'react';
import { Link } from 'react-router-dom';



function Header (){
    return (
       <div>
          <nav className="relative flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg bg-blue-300 mb-3">
            <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
              <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
                <a
                  className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-no-wrap uppercase text-white"
                  
                >

<Link to={`/glavna`}>FOTOGRAFSKI ATELJE</Link>
                  
                </a>
                <button
                  className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
                  type="button">
                  <i className="fas fa-bars"></i>
                </button>
              </div>
             
                <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
                  <li className="nav-item">
                    <a
                      className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                      
                    >
                      <i className="fab fa-facebook-square text-lg leading-lg text-white opacity-75"></i><span className="ml-2"><Link to={`/ONas`}>O NAS</Link></span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                      
                    >
                     
                    
                      <i className="fab fa-pinterest text-lg leading-lg text-white opacity-75"></i><span className="ml-2"><Link to={`/zasciteni`}>ZASCITENE FOTOGRACIJE</Link></span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                    
                    >
                      <i className="fab fa-pinterest text-lg leading-lg text-white opacity-75"></i><span className="ml-2"><Link to={`/prodani`}>PRODANE FOTOGRAFIJE</Link></span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                    
                    >
                      <i className="fab fa-pinterest text-lg leading-lg text-white opacity-75"></i><span className="ml-2"><Link to={`/dodaj`}>DODAJ FOTOGRAFIJO</Link></span>
                    </a>
                  </li>
                  
                </ul>
              </div>
          </nav>
          </div>
      );
}

export default Header;