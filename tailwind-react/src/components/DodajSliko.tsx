import React, { ChangeEvent } from 'react';
import { Link, useParams } from 'react-router-dom';

interface Okvir{
    url:string,
    Ime_Slike:string,
    Avtor:string,
    Leto:string
    Zasciteni:boolean,
    prodan:boolean,
    kategorija:string,
    dimenzije:string
}

function DodajSliko (props: {onAdd: (okvir: Okvir) => any}){


    const[url, setUrl] = React.useState<string>("");
    const[ime, setIme] = React.useState<string>("");
    const[avtor, setAvtor] = React.useState<string>("");
    const[leto, setLeto] = React.useState<string>("");
    const[kategorija, setKategorija] = React.useState<string>("");
    const[dimenzija, setDimenzija] = React.useState<string>("");


    const handleChange_ = (e: ChangeEvent<HTMLInputElement>) => {
        setUrl(e.target.value);
    }

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setIme(e.target.value);
    }

    const handleChange1 = (e: ChangeEvent<HTMLInputElement>) => {
        setAvtor(e.target.value);
    }

    const handleChange2 = (e: ChangeEvent<HTMLInputElement>) => {
        setLeto(e.target.value);
    }

    const handleChangeK = (e: ChangeEvent<HTMLInputElement>) => {
        setKategorija(e.target.value);
    }

    const handleChangeD = (e: ChangeEvent<HTMLInputElement>) => {
        setDimenzija(e.target.value);
    }


    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        let okvir: Okvir = {Ime_Slike: ime,Avtor:avtor, Leto: leto,Zasciteni:false,prodan:false,url:url,kategorija:kategorija,dimenzije:dimenzija}
        props.onAdd(okvir);
        
    }



    return (
        <main className="profile-page">
        <section className="relative block" style={{ height: "500px" }}>
          <div
            className="absolute top-0 w-full h-full bg-center bg-cover"
            style={{
              backgroundImage:
                "url('https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2710&q=80')"
            }}
          >
            <span
              id="blackOverlay"
              className="w-full h-full absolute opacity-50 bg-black"
            ></span>
          </div>
          <div
            className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
            style={{ height: "70px", transform: "translateZ(0)" }}
          >
            <svg
              className="absolute bottom-0 overflow-hidden"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="text-gray-300 fill-current"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </section>
        <section className="relative py-16 bg-gray-300">
          <div className="container mx-auto px-4">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
              <div className="px-6">
                <div className="flex flex-wrap justify-center">
                  <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                    <div className="relative">
                     
                    </div>
                  </div>
                  <div className="w-full lg:w-4/12 px-4 lg:order-3 lg:text-right lg:self-center">
                    <div className="py-6 px-3 mt-32 sm:mt-0">
                     
                    </div>
                  </div>
               
                </div>
                <div className="text-center mt-12">
                <div className="mb-3 pt-0">
<form onSubmit={handleSubmit}>

<label>
<input type="text" placeholder="Link od slike" value = {url} onChange={handleChange_} className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline"/>
  <br></br>
  <input type="text" placeholder="ime" value = {ime} onChange={handleChange} className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline"/>
  <br></br>
  <input type="text" placeholder="avtor"  value = {avtor} onChange={handleChange1} className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline"/>
 <br></br>
  <input type="text" placeholder="leto" value = {leto} onChange={handleChange2} className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline"/>
 
  <br></br>
  <input type="text" placeholder="Kategorija" value = {kategorija} onChange={handleChangeK} className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline"/>

  <br></br>
  <input type="text" placeholder="Dimenzija" value = {dimenzija} onChange={handleChangeD} className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline"/>
 
 </label>
 


<br></br>
<button type= "submit" className="py-2 px-4 text-xs mr-4 hover:bg-gray-100 text-gray-700 border-gray-300 border font-bold rounded">DODAJTE</button>
</form>
<button><Link to="/glavna">Nazaj</Link></button>
</div>			
                  
                </div>
             
              </div>
            </div>
          </div>
        </section>
      </main>

    );
}

export default DodajSliko;


/*

import React, { ChangeEvent } from 'react';
import { Link, useParams } from 'react-router-dom';

interface Igralci {
    ime:string;
    priimek:string;
    starost:number;
}


  
  interface ekipa {
    
    ime:string;
    letoUstanovitve:number;
    lokacija:string;
    igralci:Igralci[];
    
    
  } 



  

 


  

export default function DodajteEkipo (props: {onAdd: (ekipa: ekipa) => any}){

    const[ime, setIme] = React.useState<string>("");
    const[letoUstanovitve, setLetoUstanovitve] = React.useState<number>(0);
    const[lokacija, setLokacija] = React.useState<string>("");
   

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setIme(e.target.value);
    }
    const handleChange1 = (e: ChangeEvent<any>) => {
        setLetoUstanovitve(e.target.value);
    }
    const handleChange2 = (e: ChangeEvent<HTMLInputElement>) => {
        setLokacija(e.target.value);
    }

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        let ekipa: ekipa = {ime: ime,letoUstanovitve:letoUstanovitve, lokacija: lokacija, igralci: []}
        props.onAdd(ekipa);
        
    }

   


    return(
<div>
    <form onSubmit={handleSubmit}>
        <hr></hr>
        <h3>Dodajte novo ekipo:</h3>

         <label>
             <input type="text" placeholder="Ime:"                   value={ime}               onChange={handleChange} /><br></br>
             <input type="number" placeholder="Leto ustanovitve:"    value={letoUstanovitve}   onChange={handleChange1}/><br></br>
             <input type="text" placeholder="Lokacija:"              value={lokacija}          onChange={handleChange2}/><br></br>
         </label>
         <input type="submit" value="Dodaj" />
         <hr></hr>
         <button><Link to="/glavna">Nazaj na seznam ekip</Link></button>
    </form>
</div>

);


}


*/