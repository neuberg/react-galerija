
import React from 'react';

import Okvir from './components/Okvir';
import Header from './components/Header';
import Noga from './components/Noga';
import DodajSliko from './components/DodajSliko';
import { BrowserRouter as Router, Switch, Route, Redirect, Link,
  useParams } from 'react-router-dom';
import Meni from './components/Meni';
import ONas from './components/ONas';
import SeznamZascitenih from './components/SeznamZascitenih';
import SeznamProdanih from './components/SeznamProdanih';



function App() {



interface Okvir{
  url:string,
  Ime_Slike:string,
  Avtor:string,
  Leto:string
  Zasciteni:boolean,
  prodan:boolean,
  kategorija:string,
  dimenzije:string
}
 
  const [dodajvOkvir, setOkvir] = React.useState<Okvir[]>([]);

  const handleMoreChange = (Novo: Okvir[]) => {
    setOkvir(Novo);
  }

  const dodajOkvir = (okvir: Okvir) => {
    let okvirrr = Array.from(dodajvOkvir);
    okvirrr.push(okvir);
    setOkvir(okvirrr);
  }

 
 


  return (
  

 
<Router>
<div className="App">
<Header/>
<Switch>

<Route exact path="/">
          <Redirect to="/glavna" />           
</Route>

<Route path ="/glavna">
  <Meni okvirji={dodajvOkvir}/>
</Route>

<Route exact path ="/zasciteni">
  <SeznamZascitenih okvirji={dodajvOkvir} onChange={handleMoreChange}></SeznamZascitenih>
</Route>

<Route exact path ="/prodani">
  <SeznamProdanih okvirji={dodajvOkvir} onChange={handleMoreChange}></SeznamProdanih>
</Route>

<Route exact path ="/okvir/:id">
  <Okvir okvir={dodajvOkvir} onChange={handleMoreChange}/>
</Route>

<Route path ="/onas">
  <ONas/>
</Route>



<Route path ="/dodaj">
 <DodajSliko onAdd={dodajOkvir}/>
</Route>
           
<Route path ="/404">
<h1>Stran ne onstaja</h1>
</Route>

<Route path ="*">
  <Redirect to="/404" />
</Route>

</Switch>
<Noga />
</div>
</Router>
 
 


  

  );

}

export default App;
