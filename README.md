# React galerija

## Tehnologije
- **Front-end**: React TypeScript  
- **CSS knjižnica**: Tailwind

## Funkcionalnosti

- Dodajanje fotografij (skopira se naslov fotografije) (Avtor, kategorija, dimenzija..)

- Urejanje fotografij (Nove dimenzije, leto, naslov, kategorija...)

- Meni vseh fotografij z možnostjo filtriranja 

- Dodajanje fotografij na seznam zaščitenih/prodanih

- Brisanje fotografij

## Zaslonski posnetki

O nas
![O nas](slike/onas.png)

----

Dodajanje
![Dodajanje](slike/dodajanje.png)

----

Meni
![Meni](slike/meni.png)

----

Meni filtriranje
![Meni filtriranje](slike/glavna_filter.png)


----

Slika
![Slika](slike/slika.png)

----



## Namestitev

1. Kloniranje repozitorija

2. Namestitev /node_modules

    - Odpremo terminal (win + R in vtipkamo _cmd_ ter pritisnemo enter)

    - Navigirajte v mapo tailwind-react, za pomik v mapo se uporabi "cd [ime mape]", za pomik iz mape pa "."

    - V terminal vpišemo ukaz

      ```
      npm install
      ```
       ```
      npm install -D tailwindcss@latest postcss@latest autoprefixer@latest
      ```

    - Počakamo, da se namesti, vse kar je potrebno

3. Da poženemo strežnik, v terminal vpišemo ukaz
      
      ```
      npm start
      ```

4. Za dostop do strani v brskalnik prekopiramo URL 

      ```
      localhost:3000/
      ```
